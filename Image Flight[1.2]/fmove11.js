/*
============= FreehandMove Script 1.0 =============
released:        16/09/2007 (d/m/y)
compatible with: Internet Explorer 4.0+, Mozilla firefox ?
author:          Svilen Marchev - svilen.marchev@gmail.com
===================================================
*/

// ww shall store our objects here
var fmObject = new Array;
// whicj object is clicked
var clickedObjIx = -1;


// Detect if the browser is IE or not.
// If it is not IE, we assume that the browser is NS.
var IE = document.all?true:false
// If NS -- that is, !IE -- then set up for mouse capture
if (!IE) document.captureEvents(Event.MOUSEMOVE)


// Set-up to use getMouseXY function onMouseMove
document.onmousemove = fmMoveObject;


// Temporary variables to hold mouse x-y pos.s
var cursorX, cursorY;

/*
function updateMouseXY(e)
{
	if (IE) { // grab the x-y pos.s if browser is IE
		cursorX = event.clientX + document.body.scrollLeft;
		cursorY = event.clientY + document.body.scrollTop;
	} else {  // grab the x-y pos.s if browser is NS
		cursorX = e.pageX;
		cursorY = e.pageY;
	}  
	// catch possible negative values in NS4
	if (cursorX < 0){cursorX = 0;}
	if (cursorY < 0){cursorY = 0;}  
	return true;
}
*/


/*
function fmLoadObjects()
{
	// Drawing all objects with the default coordinates
	var i = 0;
	for(i in fmObject)
		fmDrawObject(fmObject[i]);
	alert("load ok");
}
*/

function fmMouseDown(objIx)
{
	objIx = parseInt(objIx);
	fmSetOActive(objIx);
	
	// Calculates the distance from the clicked object's X- and Y-position 
	//   to the X- and Y-position of the mouse by clicking  
	//updateMouseXY();
	fmObject[objIx].begX = cursorX - fmObject[objIx].x;
	fmObject[objIx].begY = cursorY - fmObject[objIx].y;
	
	clickedObjIx = objIx;
}

function fmMouseUp(objIx)
{
	objIx = parseInt(objIx);
	clickedObjIx = -1;
}

// Make a given object active:
// Sets the chosen object to top and puts the other ones under it
function fmSetOActive(objIx)
{
	objIx = parseInt(objIx);

	// Changes the z-index of some objects
	var j, i = 0;
	for(i in fmObject)
	{
		if(fmObject[i].z == fmObject[objIx].z) j = i;
		if(fmObject[i].z > fmObject[objIx].z)
			fmObject[i].z--;
	}
	fmObject[j].z = fmObject.length - 1;

	// Displays all objects with the new z-index values 
	for(i in fmObject)
		fmDrawObject(fmObject[i]);
}

function fmMoveObject(e)
{
//	if (e.button>=2)
//		return;
		
//	if (IE && event.button!=1)
//		return;
	
	// Get cursor location
	if (IE) { // grab the x-y pos.s if browser is IE
		cursorX = event.clientX + document.body.scrollLeft;
		cursorY = event.clientY + document.body.scrollTop;
	} else {  // grab the x-y pos.s if browser is NS
		cursorX = e.pageX;
		cursorY = e.pageY;
	}  
	// catch possible negative values in NS4
	if (cursorX < 0){cursorX = 0;}
	if (cursorY < 0){cursorY = 0;}  

	// Move any object, if the left mouse button has been clicked over it and the mouse has moved over it 
	var i = clickedObjIx;

	if (i != -1)
	{
		fmObject[i].x = cursorX - fmObject[i].begX;
		fmObject[i].y = cursorY - fmObject[i].begY;
		fmDrawObject(fmObject[i]);
	}
}

// I have written several drawObject functions; each of them changes different settings of the objects;
function fmDrawObject(obj)
{
	// This function changes only the X- and the Y-position
	eval(obj.id).style.top   = obj.y;
	eval(obj.id).style.left  = obj.x;
	eval(obj.id).style.zIndex = obj.z;
}

// fmObject[n] = new addFMObject( object_id )
function addFMObject(objID)
{
	this.x = parseInt(eval(objID).style.left);
	this.y = parseInt(eval(objID).style.top);
	this.z = parseInt(eval(objID).style.zIndex);
	this.begX = 0;
	this.begY = 0;
	this.id = objID;
	//this.m_click = false;
}

/*
// fmObject[n] = new addFMObject2v( object_zindex, object_id )
function addFMObject2v(objZ, objID)
{
	this.x = parseInt(eval(objID).style.left);
	this.y = parseInt(eval(objID).style.top);
	this.z = objZ;
	this.begX = 0;
	this.begY = 0;
	this.id = objID;
	this.m_click = false;

 // Apply all changes
 fmDrawObject(this);
}
*/

// fmObject[n] = new addFMObject4v( object_left, object_top, object_zindex, object_id )
function addFMObject4v(objLeft, objTop, objZ, objID)
{
	eval(objID).style.position = 'absolute';
	this.x = objLeft;
	this.y = objTop;
	this.z = objZ;
	this.begX = 0;
	this.begY = 0;
	this.id = objID;
	//this.m_click = false;
	
	// Apply all changes
	fmDrawObject(this);
	//fmDrawObject_z(this);
}
