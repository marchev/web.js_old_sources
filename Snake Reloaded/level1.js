function loadLevel1()
{
 this.load = load_0;
 this.loadItems = loadItems_0;
 this.snCheckHit = snCheckHit_0;
}

function load_0()
{
 snLevel = 1;
 snDirection = 0;
 snCurTop  = 4 + 15 * snStep;
 snCurLeft = 2 +  3 * snStep;
 snCurPartsNum = 3;
 snPartsNum = snCurPartsNum; // !!! the target value for snCurPartsNum
 snSpeed = 5;
 snMaxSpeed = 20;
 snMaxMaxSpeed = 22;
 snMinMaxSpeed = 20;
 snIncSpeed = 1;
 snDecSpeed = 2;
 snAccAfterSpeedDec = 5;
 bSnSpeedAlreadyDecreased = false;

 snStatus = 1;

 snGrndSpeed = snSpeed;

// bKeySpace = true;
 snAccI = setInterval('if(bKeyAcc) snIncreaseSpeed(snIncSpeed); else snDecreaseSpeed(snDecSpeed)', snGetSpeed());
 panSetLevel();
}

function loadItems_0()
{
 itemNum = 5;
 itemSeries[0] = 0;
 posItems[0] = new Array;
 posItems[0][0] = 0;
 posItems[0][1] = 1;
 itI[0] = setInterval("if(itemSeries[0] < 5) {loadItem(posItems[0]); itemSeries[0]++;}; else clearInterval(itI[0])", 2000);
}

// in level 1 this function is empty
function snCheckHit_0() {}