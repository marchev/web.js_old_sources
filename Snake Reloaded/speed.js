var snSpeed;
var snGrndSpeed;
var snMaxSpeed = 20;
var snMaxMaxSpeed = 22;
var snMinMaxSpeed = 20;
var snTurboSpeedDelay = 20;
var snDieselSpeedDelay = 10;
var snIncSpeed = 1;
var snDecSpeed = 2;
var snMoveI;

// If the speed has been already decreased
var bSnSpeedAlreadyDecreased = false;
var snAccAfterSpeedDec = 5;

function snGetSpeed()
{
 return( snSpeed < 21 ? (500 - snSpeed * 22) : (snSpeed == 21 ? snTurboSpeedDelay : snDieselSpeedDelay) );
}

function snIncreaseSpeed(incStep)
{
 if(snSpeed <= snMaxSpeed - incStep) snSpeed += incStep;
 panSetSpeed();
 snReSetMoveIntervals();
}
function snDecreaseSpeed(decStep)
{
 if(snSpeed == snGrndSpeed) return;
 if(snSpeed >= snGrndSpeed + decStep) snSpeed -= decStep;
 else snSpeed = snGrndSpeed;
 panSetSpeed();
 snReSetMoveIntervals();
}
function snChangeSpeed(spChange)
{
 if(snSpeed <= snMaxSpeed - spChange) snSpeed += spChange;
 else snSpeed = snMaxSpeed;
 panSetSpeed();
 snReSetMoveIntervals();
}
function snChangeMaxSpeed(spChange)
{
 if((spChange > 0 && snMaxSpeed < snMaxMaxSpeed) || (spChange < 0 && snMaxSpeed > snMinMaxSpeed))
    snMaxSpeed += spChange;
}
function snReSetMoveIntervals()
{
 if(snStatus == 1){
   clearInterval(snMoveI);
   snMoveI = setInterval("snMove()", snGetSpeed());
   clearInterval(snAccI);
   snAccI = setInterval("if(bKeyAcc) snIncreaseSpeed(snIncSpeed); else snDecreaseSpeed(snDecSpeed)", snGetSpeed());
 }
}