function snChangeLifes(snLNum)
{
 if(snLifes + snLNum >= 0){
   snLifes += snLNum;
   panSetLifes();
 }
 else gameOver();
}

function panSetLevel()
{
 id_pan_level.innerText = snLevel;
}
function panSetLifes()
{
 id_pan_lifes.innerText = snLifes;
}

function panSetPoints()
{
 id_pan_points.innerText = (snPoints < 100000 ? "0" : "")
	+ (snPoints < 10000 ? "0" : "") + (snPoints < 1000 ? "0" : "" )
	+ (snPoints < 100 ? "0" : "") + (snPoints < 10 ? "0" : "") + snPoints;
}
function panSetSpeed()
{
 id_pan_speed.innerText = (snSpeed < 10 ? "0" : "") + snSpeed;
}
function panSetLength()
{
 l = snCurPartsNum + 2;
 id_pan_length.innerText = (l > 99 ? "99+" : (l < 10 ? "0" : "") + l );
}