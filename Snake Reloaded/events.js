// the current message on the screen
var curTb = "";

function snDie_Hit()
{
 snStatus = 0;
 clearInterval(snAccI);
 clearInterval(snMoveI);
 tbMsgDisplay('id_tb_msg_1');
 bKeySpace = true;
// alert("You hit the border! :)");
}

function snDie_Bit()
{
 snStatus = 0;
 clearInterval(snAccI);
 clearInterval(snMoveI);
 tbMsgDisplay('id_tb_msg_2');
 bKeySpace = true;
// alert("The snake bit itself :)");
}

function snAlive()
{
 
}

function gamePause()
{
 snStatus = 2;
 bKeySpace = true; // spacebar - active
 clearInterval(snAccI);
 clearInterval(snMoveI);

 tbMsgDisplay('id_tb_msg_0');
}
function gameUnPause()
{
 snStatus = 1;
 bKeySpace = false;
 snReSetMoveIntervals();

 if(curTb == 'id_tb_msg_0') tbHide();
}
function gameOver()
{
 alert('Game over!');
}


function tbMsgDisplay(tbId)
{ 
 // if (there's any message already displayed) ...
 if(curTb != "") tbHide();

 curTb = tbId;
 eval(tbId).style.display = "inline";
}
function tbHide()
{
 eval(curTb).style.display = "none";
 curTb = "";
}