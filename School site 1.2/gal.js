/* The Gallery script by Svilen Marchev (McRazy) */
/* released date: 19 MAR 2004 */

// The image directory
var dir = "img/school/";
var tn = "tn_";
// All imeges we will use
var photo = new Array;
photo[0] = "pic_0039.jpg";
photo[1] = "pic_0038.jpg";
photo[2] = "pic_0041.jpg";
photo[3] = "pic_0037.jpg";

var activePic;
var photoURL;

function initGallery()
{
 id_img.src = dir + tn + photo[0];
 photoURL = dir + photo[0];
 activePic = 0;
 eval('id_mgal_' + activePic).style.textDecoration = 'underline';
}

// Used when the cursor is over/out the links of the gallery
function overGalMenu(obj){ obj.style.color = "darkred"; }
function outGalMenu(obj) { obj.style.color = "firebrick"; }

// Activate the chosen picture
function setActiveGal(picNum)
{
 eval('id_mgal_' + activePic).style.textDecoration = 'none';
 activePic = picNum;
 eval('id_mgal_' + activePic).style.textDecoration = 'underline';

 id_img.src = dir + tn + photo[activePic];
 photoURL = dir + photo[activePic];
}