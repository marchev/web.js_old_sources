/* The menu and other very important scripts */
/* By Svilen Marchev (McRazy) */

var hideDelay = 300;
var hideT;
/* The submenus of the main menus */
var subMenu = new Array(2, 4);

function init()
{
 clickMenuItem('id_startpage');
 initGallery(); // for the 'gallery' section
}

// Used when cursor is over any link of the (main) menu
function overMenu(obj)
{
 obj.style.textDecoration = "underline";
 var i = 0;
 for (i in subMenu)
  if(obj.id == "id_sec_" + subMenu[i] ){
    clearTimeout(hideT);
    displaySubMenu(subMenu[i]);
  }
}

// Used when cursor is out any link of the (main) menu
function outMenu(obj)
{
 obj.style.textDecoration = "none";
 var i = 0;
 for (i in subMenu)
  if(obj.id == "id_sec_" + subMenu[i]){
    curHideSubMenu = subMenu[i];
    hideT = setTimeout('hideSubMenu(curHideSubMenu)', hideDelay);
  }
}

// Used when cursor is over the submenu
function overSubMenu(subMObj){ clearTimeout(hideT); }

// Used when cursor is out the submenu
function outSubMenu(subMObj)
{
 var i = 0;
 for (i in subMenu)
  if(subMObj.id == "id_submenu_" + subMenu[i]){
    curHideSubMenu = subMenu[i];
    hideT = setTimeout('hideSubMenu(curHideSubMenu)', hideDelay);
  }
}

/* Used when cursor is over/out any link of the submenu */
function overSubMSec(obj) { obj.style.color = "peru"; }
function outSubMSec(obj)  { obj.style.color = "brown"; }

/* Used when cursor is over/out any link (on the start page) */
function overLink(obj) { obj.style.textDecoration = "underline"; }
function outLink(obj)  { obj.style.textDecoration = "none"; }


function clickMenuItem(objId)
{
 if(objId != ""){
   id_part_2.innerHTML = eval(objId).innerHTML;
   /* Activate the gallery menu */
   if(objId == 'id_gallery') id_gal.style.display = 'inline';
   else if(id_gal.style.display != 'none') id_gal.style.display = 'none';
 }
}

var alpF;
var displayDelay = 15;
var displayI;
var curDisSubMenu = 0;

// Display submenu
function displaySubMenu(subMNum)
{
 // Show the submenu if it is not visible
 if(eval("id_submenu_" + subMNum).style.display != "inline"){
  // If there is any activated submenu, hide it
  var i = 0;
  for(i in subMenu){
   if(eval("id_submenu_" + subMenu[i]).style.display == "inline")
     hideSubMenu(subMenu[i]);
  }
  // The menu's effect
  alpF = 30;
  eval("id_submenu_" + subMNum).style.filter = "filter: alpha(opacity=" + alpF + ", finishopacity=" + alpF + ", style=1)";
  eval("id_submenu_" + subMNum).style.display = "inline";
  curDisSubMenu = subMNum;
  displayI = setInterval('if(alpF < 90) alpF += 5; else clearInterval(displayI); eval("id_submenu_" + curDisSubMenu).style.filter = "filter: alpha(opacity=" + alpF + ", finishopacity=" + alpF + ", style=1)";', displayDelay);
 }
}

// Hide Sub menu
function hideSubMenu(subMNum){ eval("id_submenu_" + subMNum).style.display = "none"; }

// Open a new window (location = url)
function openWin(url)
{
 if(url != "") window.open(url);
}