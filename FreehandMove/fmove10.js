/*=========== FreehandMove Script 1.0 =============
released:        11/02/2004 (d/m/y)
compatible with: Internet Explorer 4.0+
author:          Svilen Marchev (McRazy)
===================================================
*/

var fmObject = new Array;

function fmLoadObjects()
{
 var i = 0;
 /* Drawing all objects with the default coordinates */
 for(i in fmObject) {
   fmDrawObject_xy(fmObject[i]);
   fmDrawObject_z (fmObject[i]);
 }
}

function fmMouseDown(obj)
{
 /* Calculates the distance from the clicked object's X- and Y-position 
    to the X- and Y-position of the mouse by clicking  */
 obj.begX = window.event.x - obj.x;
 obj.begY = window.event.y - obj.y;

 obj.m_click = true;
}

function fmMouseUp(obj)
{
 obj.m_click = false;
}

function fmSetOActive(obj)
{
 /* Sets the chosen object to top and puts the other ones under it */

 /* Changes the z-index of some objects */
 var j, i = 0;
 for(i in fmObject) {
   if(fmObject[i].z == obj.z) j = i;
   if(fmObject[i].z > obj.z)
      fmObject[i].z--;
 }
 fmObject[j].z = fmObject.length - 1;

 /* Displays all objects with the new z-index values */
 for(i in fmObject)
   fmDrawObject_z(fmObject[i]);
}

function fmMoveObject()
{
 /* Move any object, if the left mouse button has been clicked over it and the mouse has moved over it */
 var i = 0;
 for(i in fmObject)
  if(fmObject[i].m_click) {
    fmObject[i].x = window.event.x - fmObject[i].begX;
    fmObject[i].y = window.event.y - fmObject[i].begY;
    fmDrawObject_xy(fmObject[i]);
    break;
  }
}

/* I have written several drawObject functions; each of them changes different settings of the objects;
   Be sure the script will be faster now :) */
function fmDrawObject_xy(obj)
{
 /* This function changes only the X- and the Y-position */
 eval(obj.id).style.top   = obj.y;
 eval(obj.id).style.left  = obj.x;
}
function fmDrawObject_z(obj)
{
 /* Changes only the z-index */
 eval(obj.id).style.zIndex = obj.z;
}

/* fmObject[n] = new addFMObject( object_id ) */
function addFMObject(objID)
{
 this.x = parseInt(eval(objID).style.left);
 this.y = parseInt(eval(objID).style.top);
 this.z = parseInt(eval(objID).style.zIndex);
 this.begX;
 this.begY;
 this.id = objID;
 this.m_click = false;
}
/* fmObject[n] = new addFMObject2v( object_zindex, object_id ) */
function addFMObject2v(objZ, objID)
{
 this.x = parseInt(eval(objID).style.left);
 this.y = parseInt(eval(objID).style.top);
 this.z = objZ;
 this.begX;
 this.begY;
 this.id = objID;
 this.m_click = false;

 /* Apply all changes */
 fmDrawObject_z(this);
}
/* fmObject[n] = new addFMObject4v( object_left, object_top, object_zindex, object_id ) */
function addFMObject4v(objLeft, objTop, objZ, objID)
{
 eval(objID).style.position = 'absolute';
 this.x = objLeft;
 this.y = objTop;
 this.z = objZ;
 this.begX;
 this.begY;
 this.id = objID;
 this.m_click = false;

 /* Apply all changes */
 fmDrawObject_xy(this);
 fmDrawObject_z(this);
}
