# Svilen Marchev's JS sources written in high school

Most of them were written in ~2005, when I was at high school. Thus, not a good reference for code style and techniques. :)

## AplusB

Tiny widget that sums up numbers. The accent is the design.

## effect

Experiments with applying effects to images.

## 'Image Flight 1.1' and 'Image Flight[1.2]'

An app that lets you create dragonflies and move them around the screen with the mouse.

## ImageView

Image viewer.

## Mines

Minesweeper. NOTE: The latest version is in a separate repo.

## 'Snake Reloaded'

The snake game.

## BlabDatabase

A database of all the lafs (blabs, catch phrases), originated from teachers and classmates during school time.

## FreehandMove

Reusable JS library for dragging objects with the mouse.

## 'KeyCode Display'

Tiny widget that displays the code of a pressed key.

## 'Number Expasion'

A tiny widget that factors out numbers into primes.

## 'School site 1.2'

The site I wrote for my high school (12 SOU, Sofia).
